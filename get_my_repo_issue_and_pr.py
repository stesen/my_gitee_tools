#!/usr/bin/python

import requests
from load_my_gitee_config import *

def body_clean(str, j):
    if str:
        for d in j:
            str = str.replace(d, '')
        return str
    return "none"

def time_clean(str, j):
    if str:
        for d in j:
            str = str.replace(d, '')
        return str.replace('T', ' ')
    return "none"

cfg = load_gitee_config_and_update_token()
org = cfg.get('org')
token = cfg.get('access_token')

my_repo_pattern = cfg.get('my_repo_pattern')
if not my_repo_pattern:
    my_repo_pattern = ""

body_del = cfg.get('body_del')
if not body_del:
    body_del = []

time_del = cfg.get('time_del')
if not time_del:
    time_del = []

if len(org) == 0 or len(token) == 0:
    quit();

headers = { 'Content-Type': 'application/json',
        'charset' : 'UTF-8' }

all_name=[]
page = 0
while True:
    page += 1
    url="https://gitee.com/api/v5/orgs/{:s}/repos".format(org)
    params = {'access_token' : token,
            'type' : 'all',
            'page' : page,
            'per_page' : '100'}
    r = requests.get(url = url, headers = headers, params = params)
    data = r.json()
    if len(data) == 0:
        break

    for d in data:
        if my_repo_pattern in d['name']:
            all_name.append(d['name'])

sum = len(all_name)
print("get {:} repos".format(sum))
for name in all_name:
    issue_cnt = 0
    pr_cnt = 0
    issue_url="https://gitee.com/api/v5/repos/{:s}/{:s}/issues".format(org, name)
    pr_url="https://gitee.com/api/v5/repos/{:s}/{:s}/pulls".format(org, name)

    print("\nREPO: {:}".format(name))
    page = 0
    while True:
        page += 1
        params = { 'state' : 'open',
                'sort' : 'created',
                'direction' : 'desc',
                'page' : page,
                'per_page' : '20'}
        issue_r = requests.get(url = issue_url, params = params, headers = headers)
        data = issue_r.json()
        if len(data) == 0:
            break
        for d in data:
            issue_cnt += 1
            print("  ISSUE {:}, Comments: {:}, create: {:}, update:{:}".format(issue_cnt, d['comments'], time_clean(d['created_at'], time_del), time_clean(d['updated_at'], time_del)))
            print("    title: {:} ,body: {:}".format(d['title'][:32], body_clean(d['body'], body_del)[:32]))
            print("    url: {:}".format(d['html_url']))
    if issue_cnt == 0:
        print("  No Open Issue")
    else:
        print("  ------------------------------")

    page = 0
    while True:
        page += 1
        params = { 'state' : 'open',
                'sort' : 'created',
                'direction' : 'desc',
                'page' : page,
                'per_page' : '20'}
        pr_r = requests.get(url = pr_url, params = params, headers = headers)
        data = pr_r.json()
        if len(data) == 0:
            break
        for d in data:
            pr_cnt += 1
            assignees = []
            for a in d['assignees']:
                assignees.append(a['login'])
            print("  PR {:}, assignees: {:}, create: {:}, update:{:}".format(pr_cnt, assignees, time_clean(d['created_at'], time_del), time_clean(d['updated_at'], time_del)))
            print("    title: {:} ,body: {:}".format(d['title'][:32], body_clean(d['body'], body_del)[:32]))
            print("    url: {:}".format(d['html_url']))
    if pr_cnt == 0:
        print("  No Open PR")
    else:
        print("  ------------------------------")


