#!/usr/bin/python

import requests
from load_my_gitee_config import *

cfg = load_gitee_config_and_update_token()
org = cfg.get('org')
token = cfg.get('access_token')

if len(org) == 0 or len(token) == 0:
    quit();

headers= { 'Content-Type': 'application/json',
        'charset' : 'UTF-8' }

all_name=[]
for i in range(1, 100):
    url="https://gitee.com/api/v5/orgs/{:s}/repos".format(org)
    params = {'access_token' : token,
            'type' : 'all',
            'page' : i,
            'per_page' : '100'}
    r = requests.get(url = url, headers = headers, params = params)
    data = r.json()
    if len(data) == 0:
        break

    for d in data:
        all_name.append(d['name'])

cnt = 0
sum = len(all_name)
print("get {:} repos".format(sum))
params = {"access_token" : token}
for name in all_name:
    cnt += 1
    url="https://gitee.com/api/v5/user/starred/{:s}/{:s}".format(org, name)
    r = requests.put(url = url, params = params, headers = headers)
    print("{:}/{:}: {:} {:}".format(cnt, sum, name, r))
